import { Component, ViewChild } from '@angular/core';
import { DetailsComponent } from './details/details.component';
import { EditPizza } from './master/master.component';
import { Pizza } from './pizza';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @ViewChild('details', { static: true }) details: DetailsComponent;
  pizzasCollection: Pizza[] = [];
  editIndex: number = null;
  setPizza(pizza: Pizza) {
    if (pizza.isEditing) {
      pizza.isEditing = false;
      this.pizzasCollection[this.editIndex] = pizza;
      this.editIndex = null;
      return this.updateView();
    }
    this.pizzasCollection = [...this.pizzasCollection, pizza];
  }

  editPizza({ pizza, index }: EditPizza) {
    pizza.isEditing = false;
    this.pizzasCollection[index].isEditing = true;
    this.editIndex = index;
    this.details.pizzaForm.setValue(pizza);
    this.updateView();
  }

  resetCollection() {
    this.pizzasCollection = [];
  }

  updateView() {
    this.pizzasCollection = [...this.pizzasCollection];
    console.log(this.pizzasCollection);
  }

  jsonPizzaCollection() {
    return JSON.stringify(this.pizzasCollection);
  }
}
