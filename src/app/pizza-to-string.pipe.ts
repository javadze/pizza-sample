import { Pipe, PipeTransform } from '@angular/core';
import { Pizza } from './pizza';

@Pipe({
  name: 'pizzaToString',
})
export class PizzaToStringPipe implements PipeTransform {
  transform(value: Pizza): string {
    console.log(value);

    return `${value.pizza}-${value.country}`;
  }
}
