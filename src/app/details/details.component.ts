import {
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChildren,
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Pizza } from '../pizza';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent {
  pizzaForm = new FormGroup({
    pizza: new FormControl(''),
    country: new FormControl(''),
    isEditing: new FormControl(false),
  });
  @ViewChildren('pizza') pizzaName: any;

  @Output() pizza = new EventEmitter<Pizza>();
  @Output() reset = new EventEmitter<null>();

  constructor() {}

  onSubmit() {
    this.pizza.emit(this.pizzaForm.value);
    this.pizzaForm.reset();
    this.pizzaName.first.nativeElement.focus();
  }
}
