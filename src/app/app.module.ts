import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PizzaToStringPipe } from './pizza-to-string.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { MasterComponent } from './master/master.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    MasterComponent,
    DetailsComponent,
    PizzaToStringPipe,
  ],
  imports: [BrowserModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
