import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { Pizza } from '../pizza';
export type EditPizza = { pizza: Pizza; index: number };
@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MasterComponent implements OnInit {
  @Input() pizzas: Pizza[] = [];
  @Output() edit = new EventEmitter<EditPizza>();
  constructor() {}

  ngOnInit(): void {}

  editPizza(pizza: Pizza, index: number) {
    this.edit.emit({ pizza, index });
  }
}
