import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Pizza } from './pizza';

describe('AppComponent', () => {
  const testPizza: Pizza = {
    pizza: 'test-pizza',
    country: 'test-country',
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('title').textContent).toContain(
      'I use Tdd so use ng test and ng e2e after you see this beauty'
    );
  });
});
