export interface Pizza {
  pizza: string;
  country: string;
  isEditing: boolean;
}
